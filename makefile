.PHONY: install restart help pass check rm build push q ansible
ansible: ## Decrypt .env
	sudo docker build -t ansib .
	sudo docker run -i -v $$(pwd):/home/an/ --rm ansib ansible-vault decrypt --output /home/an/.env /home/an/ansible/.env.encrypt --vault-password-file=/home/an/vault.pass
	sudo docker run -i -v $$(pwd):/home/an/ --rm ansib ansible-vault decrypt --output /home/an/pass /home/an/ansible/pass.encrypt --vault-password-file=/home/an/vault.pass
install: ## Install WP
	sudo docker-compose up -d
check: ## Check repository
	sudo git pull -v
help: ## Command list
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS= ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
restart: ## Restar docker-compose conteiners
	sudo docker-compose restart
pass: ## Password
	sudo cat pass
uninstall: ## Uninstall conteiners
	sudo docker-compose stop
	sudo docker rm -v db wordpress webserver
	sudo docker rmi nginx:1.15.12-alpine mysql:8.0 wordpress:5.1.1-fpm-alpine
rm: ## Delite service
	docker service rm node_db node_wordpress node_webserver node_grafana node_promtail node_prometheus node_loki node_nginx-exporter

build: ## Build dockerfile containers
	cp -r mysql db
	cp -r html nginx-conf && cp -r html wp

	docker build -t swarm-nginx:latest ./nginx-conf --no-cache
	docker tag swarm-nginx:latest vladshezh/docker:nginx

	docker build -t swarm-mysql:latest ./db --no-cache
	docker tag swarm-mysql:latest vladshezh/docker:mysql

	docker build -t swarm-wp:latest ./wp --no-cache
	docker tag swarm-wp:latest vladshezh/docker:wp

	docker build -t swarm-grafana:latest ./grafana --no-cache
	docker tag swarm-grafana:latest vladshezh/docker:grafana

	docker build -t swarm-loki:latest ./loki --no-cache
	docker tag swarm-loki:latest vladshezh/docker:loki 

	docker build -t swarm-prometheus:latest ./prometheus --no-cache
	docker tag swarm-prometheus:latest vladshezh/docker:prometheus

	docker build -t swarm-promtail:latest ./promtail --no-cache
	docker tag swarm-promtail:latest vladshezh/docker:promtail

push: ## Push to dockerhub
	docker push vladshezh/docker:nginx
	docker push vladshezh/docker:mysql
	docker push vladshezh/docker:wp
	docker push vladshezh/docker:grafana
	docker push vladshezh/docker:loki
	docker push vladshezh/docker:prometheus
	docker push vladshezh/docker:promtail

swarm: 
	sudo docker stack up --with-registry-auth -c docker-compose-swarm.yml node

q:
	make build && make push
